define({ "api": [
  {
    "type": "Delete",
    "url": "/api/v1/addresses{:id}",
    "title": "7-Delete an existing address",
    "version": "0.3.0",
    "name": "DeleteAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE \\\nhttp://localhost:3000/api/v1/addresses/4 \\",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \"Address was successfully destroyed\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AddressNotFound",
            "description": "<p>The id of the address was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"AddressNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "Get",
    "url": "/api/v1/delete_user_addresses?{:user_id}",
    "title": "8-DELETE all addresses of a specific user",
    "version": "0.3.0",
    "name": "DeleteUserAddresses",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/delete_user_addresses?1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>user ID than you want to delete all his addresses.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  },\n  {\n      \"id\": 5,\n      \"user_id\": 1,\n      \"address\": \"جده شارع خالد ابن الوليد\",\n      \"country\": \"SA\",\n      \"governorate\": \"جده\",\n      \"city\": \"الدخيله\",\n      \"street\": \"شارع خالد ابن الوليد\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T11:15:30.747Z\",\n      \"updated_at\": \"2018-09-12T11:15:30.747Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>There are no results for this user id.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 User Not Found\n  {\n    \"error\": \"This user was not found in our database\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/addresses",
    "title": "1-Request addresses List",
    "version": "0.3.0",
    "name": "GetAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/addresses",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 2,\n      \"address\": \"المنصوره طلخا دقهليه\",\n      \"country\": \"EG\",\n      \"governorate\": \"دقهلية\",\n      \"city\": \"طلخا\",\n      \"street\": \"شارع البحر الاعظم\",\n      \"default_address\": true,\n      \"created_at\": \"2018-09-12T10:54:22.468Z\",\n      \"updated_at\": \"2018-09-12T11:28:17.326Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/search_address?&search={:search}&search_user={:user_id}&address_status={:default_address}&searchdatefrom={:created_at_from}&searchdateto={:created_at_to}",
    "title": "6-Search for an existing Address",
    "version": "0.3.0",
    "name": "GetAddressSearch",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/search_address?=&search=100&search_user=1&address_status=1&searchdatefrom=2018-09-11&searchdateto=2018-09-13",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>search word that you want to find(address,country,governorate,city,street).</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "searchdatefrom",
            "description": "<p>search created at date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "searchdateto",
            "description": "<p>search created at date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "address_status",
            "description": "<p>search default address status than you want(1 for Defualt , 0 for Not Default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "search_user",
            "description": "<p>search user ID than you want.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoResults",
            "description": "<p>There are no results for your search.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 No Results\n  {\n    \"error\": \"There are no results for your search.\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/addresses/{:id}",
    "title": "2-Request Specific Address",
    "version": "0.3.0",
    "name": "GetSpecificAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/addresses/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"user_id\": 1,\n  \"address\": \"New Damiatta , el-mahgob,service high\",\n  \"country\": \"EG\",\n  \"governorate\": \"Damiatta\",\n  \"city\": \"New Damiatta\",\n  \"street\": \"el-mahgob\",\n  \"default_address\": false,\n  \"created_at\": \"2018-09-12T10:48:29.946Z\",\n  \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AddressNotFound",
            "description": "<p>The id of the address was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"AddressNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/user_addresses",
    "title": "3-Request Specific User Addresses",
    "version": "0.3.0",
    "name": "GetSpecificUserAddresses",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_addresses",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  },\n  {\n      \"id\": 5,\n      \"user_id\": 1,\n      \"address\": \"جده شارع خالد ابن الوليد\",\n      \"country\": \"SA\",\n      \"governorate\": \"جده\",\n      \"city\": \"الدخيله\",\n      \"street\": \"شارع خالد ابن الوليد\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T11:15:30.747Z\",\n      \"updated_at\": \"2018-09-12T11:15:30.747Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The id of the user was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"UserNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "post",
    "url": "/api/v1/addresses",
    "title": "4-Create a new address",
    "version": "0.3.0",
    "name": "PostAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/addresses \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"user_id\": 3,\n  \"address\": \"mansoura,talkha\",\n  \"country\": \"EG\",\n  \"governorate\": \"Daqahliyah\",\n  \"city\": \"mansoura\",\n  \"street\": \"talkha,om kalthom street\",\n  \"default_address\": false\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 7,\n        \"user_id\": 3,\n        \"address\": \"mansoura,talkha\",\n        \"country\": \"EG\",\n        \"governorate\": \"Daqahliyah\",\n        \"city\": \"mansoura\",\n        \"street\": \"talkha,om kalthom street\",\n        \"default_address\": false,\n        \"created_at\": \"2018-09-12T11:15:30.747Z\",\n        \"updated_at\": \"2018-09-12T11:15:30.747Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAddress",
            "description": "<p>address must be more or equal than 5 letters.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Invalid Address\n  {\n    \"error\": \"must be more or equal than 5 letters\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "Put",
    "url": "/api/v1/addresses{:id}",
    "title": "5-Update an existing address",
    "version": "0.3.0",
    "name": "PutAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/addresses/7 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"address\": \"mansoura,talkha\",\n  \"country\": \"EG\",\n  \"governorate\": \"Daqahliyah\",\n  \"city\": \"mansoura\",\n  \"street\": \"talkha,om kalthom street\",\n  \"default_address\": true\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 7,\n        \"user_id\": 3,\n        \"address\": \"mansoura,talkha\",\n        \"country\": \"EG\",\n        \"governorate\": \"Daqahliyah\",\n        \"city\": \"mansoura\",\n        \"street\": \"talkha,om kalthom street\",\n        \"default_address\": true,\n        \"created_at\": \"2018-09-12T11:15:30.747Z\",\n        \"updated_at\": \"2018-09-12T13:14:21.335Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AddressNotFound",
            "description": "<p>The id of the address was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAddress",
            "description": "<p>address must be more or equal than 5 letters.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"AddressNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Address\n  {\n    \"error\": \"must be more or equal than 5 letters\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/buy_card?val={:value}",
    "title": "7-Buy a Card",
    "version": "0.3.0",
    "name": "BuyCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/buy_card?val=1000",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status(1 for active).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 23,\n  \"number\": 1650927974431984,\n  \"value\": 1000,\n  \"status\": 1,\n  \"expired_at\": \"2018-07-31\",\n  \"user_id\": 2,\n  \"invoice_id\": 0,\n  \"created_at\": \"2018-07-28T15:06:25.684Z\",\n  \"updated_at\": \"2018-07-29T08:41:02.510Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardWasNotCreated",
            "description": "<p>something went wrong while saving this card.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"Card Was Not Created\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/cards",
    "title": "1-Request cards List",
    "version": "0.3.0",
    "name": "GetCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n        \"id\": 5,\n        \"number\": 1234567891234566,\n        \"value\": 150,\n        \"status\": 3,\n        \"expired_at\": \"2018-07-24\",\n        \"user_id\": 2,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-24T12:50:56.749Z\",\n        \"updated_at\": \"2018-07-24T12:51:18.567Z\"\n    },\n    {\n        \"id\": 6,\n        \"number\": 5722704937640978,\n        \"value\": 200,\n        \"status\": 1,\n        \"expired_at\": \"2022-06-25\",\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-24T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-24T13:24:23.665Z\"\n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/search?&search={:search_word}&search_user={:user_id}&status={:status}&searchdatefrom={:expired_at_from}&searchdateto={:expired_at_to}",
    "title": "5-Search for an existing Card",
    "version": "0.3.0",
    "name": "GetCardSearch",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/search?=&search=100&search_user=1&status=1&searchdatefrom=2018-07-24&searchdateto=2018-07-25",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "search_word",
            "description": "<p>card search number or value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status(Active ,Charged ,pending ,disabled).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card User ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at_from",
            "description": "<p>card search expired Date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at_to",
            "description": "<p>card search expired Date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 2,\n  \"number\": 1234567891234567,\n  \"value\": 120,\n  \"status\": 1,\n  \"expired_at\": \"2018-07-25\",\n  \"user_id\": 1,\n  \"invoice_id\": 0,\n  \"created_at\": \"2018-07-24T12:43:16.492Z\",\n  \"updated_at\": \"2018-07-25T14:01:18.051Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoResults",
            "description": "<p>There are no results for your search.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 No Results\n  {\n    \"error\": \"NoResults\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/cards/{:id}",
    "title": "2-Request Specific Card",
    "version": "0.3.0",
    "name": "GetSpecificCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 1,\n        \"number\": 5722702457640936,\n        \"value\": 100,\n        \"status\": 2,\n        \"expired_at\": \"2022-05-20\",\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-24T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-24T13:24:23.665Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The id of the Card was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CardNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "post",
    "url": "/api/v1/cards",
    "title": "3-Create a new Card",
    "version": "0.3.0",
    "name": "PostCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/cards \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"number\": 1111111122222222,\n  \"value\": 300,\n  \"status\": 1,\n  \"expired_at\": \"2018-07-26\",\n  \"user_id\": 3,\n  \"invoice_id\": 0\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card number must be 16 digit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card user ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 7,\n        \"number\": 1178702457640936,\n        \"value\": 500,\n        \"status\": 2,\n        \"expired_at\": \"2022-05-20\",\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-25T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-25T13:24:23.665Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingNumber",
            "description": "<p>card number has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidNumber",
            "description": "<p>card number not equal 16 digit.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing Number\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Number\n  {\n    \"error\": \"must be 16 digit long\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "put",
    "url": "/api/v1/cards/{:id}",
    "title": "4-Update an existing Card",
    "version": "0.3.0",
    "name": "PutCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/cards/6 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"value\": 400,\n  \"status\": 2,\n  \"expired_at\": \"2018-07-26\",\n  \"user_id\": 3\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card user ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 6,\n        \"number\": 1155463457640936,\n        \"value\": 200,\n        \"status\": 2,\n        \"expired_at\": \"2022-05-20\",\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-25T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-25T13:24:23.665Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The id of the Card was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CardNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_status?id={:id}",
    "title": "6-Suspend a Specific Card",
    "version": "0.3.0",
    "name": "SuspendCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_status?id=22",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status(1 for enable &amp; 4 for suspend).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": 1,\n  \"id\": 22,\n  \"number\": 8250409147380401,\n  \"value\": 200,\n  \"expired_at\": \"2018-07-31\",\n  \"user_id\": 1,\n  \"invoice_id\": 0,\n  \"created_at\": \"2018-07-28T10:07:53.960Z\",\n  \"updated_at\": \"2018-07-29T08:29:33.484Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The ID of this Card was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Card Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "Delete",
    "url": "/api/v1/cards_categories/{:id}",
    "title": "6-Delete an existing Card Category",
    "version": "0.3.0",
    "name": "DeleteCardCategory",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE \\\nhttp://localhost:3000/api/v1/cards_categories/5 \\",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card category unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"card_value\": 400,\n  \"active\": 1,\n  \"order\": 5,\n  \"created_at\": \"2018-07-28T12:37:11.483Z\",\n  \"updated_at\": \"2018-07-28T15:24:10.303Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Card Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response4:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CategoryNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_categories",
    "title": "1-Request cards categories List",
    "version": "0.3.0",
    "name": "GetCardsCategories",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_categories",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n{\n    \"id\": 1,\n    \"card_value\": 100,\n    \"active\": 1,\n    \"order\": 1,\n    \"created_at\": \"2018-07-26T14:41:50.506Z\",\n    \"updated_at\": \"2018-07-28T08:03:16.400Z\"\n},\n{\n  \"id\": 3,\n  \"card_value\": 300,\n  \"active\": 0,\n  \"order\": 3,\n  \"created_at\": \"2018-07-26T14:49:06.665Z\",\n  \"updated_at\": \"2018-07-28T08:38:59.747Z\"\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/display_cards_categories",
    "title": "2-Request displayed cards categories List",
    "version": "0.3.0",
    "name": "GetDisplayedCardsCategories",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/display_cards_categories",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n{\n    \"id\": 1,\n    \"card_value\": 100,\n    \"active\": 1,\n    \"order\": 1,\n    \"created_at\": \"2018-07-26T14:41:50.506Z\",\n    \"updated_at\": \"2018-07-28T08:03:16.400Z\"\n},\n{\n    \"id\": 2,\n    \"card_value\": 200,\n    \"active\": 1,\n    \"order\": 2,\n    \"created_at\": \"2018-07-26T14:48:50.514Z\",\n    \"updated_at\": \"2018-07-28T08:38:53.795Z\"\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_categories/{:id}",
    "title": "3-Request Specific card category",
    "version": "0.3.0",
    "name": "GetSpecificCardsCategories",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_categories/3",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card Category unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 3,\n  \"card_value\": 300,\n  \"active\": 0,\n  \"order\": 3,\n  \"created_at\": \"2018-07-26T14:49:06.665Z\",\n  \"updated_at\": \"2018-07-28T08:38:59.747Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Card Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CategoryNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "post",
    "url": "/api/v1/cards_categories",
    "title": "4-Create a new Card Category",
    "version": "0.3.0",
    "name": "PostCardCategory",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/cards_categories \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"card_value\": 500,\n  \"active\": 1,\n  \"order\": 5,\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category unique order(high order for high category value).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"card_value\": 500,\n  \"active\": 0,\n  \"order\": 5,\n  \"created_at\": \"2018-07-28T12:37:11.483Z\",\n  \"updated_at\": \"2018-07-28T12:37:11.483Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingValue",
            "description": "<p>card category value has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOrder",
            "description": "<p>card category order has already been taken.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing Category Value\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Order\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "post",
    "url": "/api/v1/cards_categories/{:id}",
    "title": "5-Update an existing Card Category",
    "version": "0.3.0",
    "name": "PutCardCategory",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/cards_categories/5 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"card_value\": 400,\n  \"active\": 1,\n  \"order\": 5,\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category unique order(high order for high category value).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"card_value\": 400,\n  \"active\": 1,\n  \"order\": 5,\n  \"created_at\": \"2018-07-28T12:37:11.483Z\",\n  \"updated_at\": \"2018-07-28T15:24:10.303Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingValue",
            "description": "<p>card category value has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOrder",
            "description": "<p>card category order has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Card Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing Category Value\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Order\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response4:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CategoryNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_logs",
    "title": "1-Request card log List",
    "version": "0.3.0",
    "name": "GetCardLog",
    "group": "Cards_Logs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_logs",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card log unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card unique user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "action_type",
            "description": "<p>card log type (1 for created ,2 for charged ,3 for deleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ip",
            "description": "<p>card log user interface ip.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_id",
            "description": "<p>card unique id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"action_type\": 2,\n      \"ip\": \"127.0.0.1\",\n      \"created_at\": \"2018-09-09T08:10:43.950Z\",\n      \"updated_at\": \"2018-09-09T08:10:43.950Z\",\n      \"card_id\": 36\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 1,\n      \"action_type\": 3,\n      \"ip\": \"127.0.0.1\",\n      \"created_at\": \"2018-09-09T08:13:16.518Z\",\n      \"updated_at\": \"2018-09-09T08:13:16.518Z\",\n      \"card_id\": 5\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_logs_controller.rb",
    "groupTitle": "Cards_Logs"
  },
  {
    "type": "get",
    "url": "/api/cards_logs/{:id}",
    "title": "2-Request Specific Card Log",
    "version": "0.3.0",
    "name": "GetSpecificCardLog",
    "group": "Cards_Logs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_logs/2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card log unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card unique user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "action_type",
            "description": "<p>card log type (1 for created ,2 for charged ,3 for deleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ip",
            "description": "<p>card log user interface ip.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_id",
            "description": "<p>card unique id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 2,\n  \"user_id\": 1,\n  \"action_type\": 1,\n  \"ip\": \"127.0.0.1\",\n  \"created_at\": \"2018-09-09T07:52:48.179Z\",\n  \"updated_at\": \"2018-09-09T07:52:48.179Z\",\n  \"card_id\": 36\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardLogNotFound",
            "description": "<p>The id of the Card log was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CardLogNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/cards_logs_controller.rb",
    "groupTitle": "Cards_Logs"
  },
  {
    "type": "get",
    "url": "/api/v1/money_ops",
    "title": "1-Request Money Operations List",
    "version": "0.3.0",
    "name": "GetMoneyOperation",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/money_ops",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"opid\": \"PYZPRV18M7NT\",\n      \"optype\": 1,\n      \"amount\": \"150\",\n      \"payment_gateway\": 1,\n      \"status\": 0,\n      \"payment_date\": \"2018-08-01T09:52:00.000Z\",\n      \"payment_id\": 3,\n      \"user_id\": 1,\n      \"created_at\": \"2018-08-01T09:52:20.462Z\",\n      \"updated_at\": \"2018-08-01T09:52:20.462Z\"\n  },\n  {\n      \"id\": 2,\n      \"opid\": \"PYTUGN80WB39\",\n      \"optype\": 1,\n      \"amount\": \"50\",\n      \"payment_gateway\": 1,\n      \"status\": 0,\n      \"payment_date\": \"2018-08-01T09:52:00.000Z\",\n      \"payment_id\": 2,\n      \"user_id\": 1,\n      \"created_at\": \"2018-08-01T09:52:56.776Z\",\n      \"updated_at\": \"2018-08-01T09:52:56.776Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "get",
    "url": "/api/money_ops/{:id}",
    "title": "2-Request Specific Money Operation",
    "version": "0.3.0",
    "name": "GetSpecificMoneyOperation",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/money_ops/2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card Operation ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 2,\n      \"opid\": \"PYTUGN80WB39\",\n      \"optype\": 1,\n      \"amount\": \"50\",\n      \"payment_gateway\": 1,\n      \"status\": 0,\n      \"payment_date\": \"2018-08-01T09:52:00.000Z\",\n      \"payment_id\": 2,\n      \"user_id\": 1,\n      \"created_at\": \"2018-08-01T09:52:56.776Z\",\n      \"updated_at\": \"2018-08-01T09:52:56.776Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The id of this Money Operation was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Card Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "post",
    "url": "/api/v1/money_ops",
    "title": "3-Create a new Money Operation",
    "version": "0.3.0",
    "name": "PostMoneyOperation",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/money_ops \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n\"optype\": 1,\n\"amount\": \"150\",\n\"payment_gateway\": 1,\n\"status\": 0,\n\"payment_date\": \"2018-08-01T13:31:00.000Z\",\n\"payment_id\": 3,\n\"user_id\": 1\n}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number (Created automatically and must be 12 digits and letters ).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 8,\n  \"opid\": \"PYL42W60CTGM\",\n  \"optype\": 1,\n  \"amount\": \"150\",\n  \"payment_gateway\": 1,\n  \"status\": 0,\n  \"payment_date\": \"2018-08-01T13:31:00.000Z\",\n  \"payment_id\": 3,\n  \"user_id\": 1,\n  \"created_at\": \"2018-08-01T11:33:49.055Z\",\n  \"updated_at\": \"2018-08-01T11:33:49.055Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOperationId",
            "description": "<p>Operation Id has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 Existing Operation Id\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "get",
    "url": "/api/v1/operations_status?id={:id}",
    "title": "4-Update a Specific Operation Status",
    "version": "0.3.0",
    "name": "UpdateOperationStatus",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/operations_status?id=5",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": 1,\n  \"id\": 8,\n  \"opid\": \"PYL42W60CTGM\",\n  \"optype\": 1,\n  \"amount\": \"150\",\n  \"payment_gateway\": 1,\n  \"payment_date\": \"2018-08-01T13:31:00.000Z\",\n  \"payment_id\": 3,\n  \"user_id\": 1,\n  \"created_at\": \"2018-08-01T11:33:49.055Z\",\n  \"updated_at\": \"2018-08-01T15:45:10.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The ID of this Mony Operation was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Card Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "Delete",
    "url": "/api/v1/notifications{:id}",
    "title": "5-Delete an existing notification",
    "version": "0.3.0",
    "name": "DeleteNotification",
    "group": "Notifications",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE \\\nhttp://localhost:3000/api/v1/notifications/4 \\",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>the notification unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \"Notification was successfully destroyed\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotificationNotFound",
            "description": "<p>The id of the notification was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Notification Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_controller.rb",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "/api/v1/notifications/{:id}",
    "title": "2-Request a specific notification",
    "version": "0.3.0",
    "name": "GetNotification",
    "group": "Notifications",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/notifications/2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification user unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>notification title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>notification content.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "notification_type",
            "description": "<p>notification service type(1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 2,\n      \"user_id\": 2,\n      \"title\": \"Balance transfer\",\n      \"description\": \"5 USD has been successfully transferred to user 1\",\n      \"notification_type\": 1,\n      \"created_at\": \"2018-09-16T11:48:44.618Z\",\n      \"updated_at\": \"2018-09-16T11:48:44.618Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotificationNotFound",
            "description": "<p>The id of this notification was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Notification Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_controller.rb",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "/api/v1/search_notifications?&search={:search}&search_user={:user_id}&notification_type={:notification_type}&searchdatefrom={:created_at_from}&searchdateto={:created_at_to}",
    "title": "4-Search for an existing notification",
    "version": "0.3.0",
    "name": "GetNotificationSearch",
    "group": "Notifications",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/search_notifications?=&search=balance&search_user=1&notification_type=1&searchdatefrom=2018-09-16&searchdateto=2018-09-17",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>search word that you want to find(title , description).</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "searchdatefrom",
            "description": "<p>search created at date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "searchdateto",
            "description": "<p>search created at date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "notification_type",
            "description": "<p>notification service type(1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "search_user",
            "description": "<p>search user ID than you want.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification user unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>notification title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>notification content.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "notification_type",
            "description": "<p>notification service type(1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"title\": \"Balance transfer\",\n      \"description\": \"10 USD has been successfully transferred to user 2\",\n      \"notification_type\": 3,\n      \"created_at\": \"2018-09-16T11:46:40.994Z\",\n      \"updated_at\": \"2018-09-16T11:46:40.994Z\"\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 2,\n      \"title\": \"Balance transfer\",\n      \"description\": \"5 USD has been successfully transferred to user 1\",\n      \"notification_type\": 1,\n      \"created_at\": \"2018-09-16T11:48:44.618Z\",\n      \"updated_at\": \"2018-09-16T11:48:44.618Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoResults",
            "description": "<p>There are no results for your search.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 No Results\n  {\n    \"error\": \"There are no results for your search.\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_controller.rb",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "/api/v1/notifications",
    "title": "1-Request notifications List",
    "version": "0.3.0",
    "name": "GetNotifications",
    "group": "Notifications",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/notifications",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification user unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>notification title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>notification content.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "notification_type",
            "description": "<p>notification service type (1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"title\": \"Balance transfer\",\n      \"description\": \"10 USD has been successfully transferred to user 2\",\n      \"notification_type\": 3,\n      \"created_at\": \"2018-09-16T11:46:40.994Z\",\n      \"updated_at\": \"2018-09-16T11:46:40.994Z\"\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 2,\n      \"title\": \"Balance transfer\",\n      \"description\": \"5 USD has been successfully transferred to user 1\",\n      \"notification_type\": 1,\n      \"created_at\": \"2018-09-16T11:48:44.618Z\",\n      \"updated_at\": \"2018-09-16T11:48:44.618Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_controller.rb",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "/api/v1/user_notifications",
    "title": "3-Request a notifications list of a specific user",
    "version": "0.3.0",
    "name": "GetUserNotification",
    "group": "Notifications",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_notifications",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification user unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification user unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>notification title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>notification content.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "notification_type",
            "description": "<p>notification service type(1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 4,\n      \"user_id\": 1,\n      \"title\": \"Balance transfer\",\n      \"description\": \"5 USD has been successfully transferred to user 2\",\n      \"notification_type\": 3,\n      \"created_at\": \"2018-09-16T12:38:39.493Z\",\n      \"updated_at\": \"2018-09-16T12:38:39.493Z\"\n  },\n  {\n      \"id\": 5,\n      \"user_id\": 1,\n      \"title\": \"Balance transfer\",\n      \"description\": \"10 USD has been successfully transferred to user 2\",\n      \"notification_type\": 3,\n      \"created_at\": \"2018-09-16T13:21:44.439Z\",\n      \"updated_at\": \"2018-09-16T13:21:44.439Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The id of this User was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"User Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_controller.rb",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "/api/v1/notifications_settings",
    "title": "1-Request notifications settings List",
    "version": "0.3.0",
    "name": "GetNotificationsSettings",
    "group": "Notifications_Settings",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/notifications_settings",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification setting unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification setting user ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "money_transactions",
            "description": "<p>Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pending_transactions",
            "description": "<p>pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "transactions_updates",
            "description": "<p>current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "help_tickets_updates",
            "description": "<p>help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tickets_replies",
            "description": "<p>new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "account_login",
            "description": "<p>login to user account(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "change_password",
            "description": "<p>change password(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "verifications_setting",
            "description": "<p>when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"money_transactions\": 3,\n      \"pending_transactions\": 1,\n      \"transactions_updates\": 1,\n      \"help_tickets_updates\": 3,\n      \"tickets_replies\": 1,\n      \"account_login\": 0,\n      \"change_password\": 1,\n      \"verifications_setting\": 3,\n      \"created_at\": \"2018-08-09T11:20:01.741Z\",\n      \"updated_at\": \"2018-08-09T11:20:01.741Z\"\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 2,\n      \"money_transactions\": 1,\n      \"pending_transactions\": 2,\n      \"transactions_updates\": 1,\n      \"help_tickets_updates\": 1,\n      \"tickets_replies\": 3,\n      \"account_login\": 0,\n      \"change_password\": 0,\n      \"verifications_setting\": 2,\n      \"created_at\": \"2018-08-09T11:21:11.770Z\",\n      \"updated_at\": \"2018-08-09T11:22:21.779Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_settings_controller.rb",
    "groupTitle": "Notifications_Settings"
  },
  {
    "type": "post",
    "url": "/api/v1/notifications_settings",
    "title": "2-Create a Notification Settings",
    "version": "0.3.0",
    "name": "PostNotificationsSettings",
    "group": "Notifications_Settings",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/notifications_settings \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n\"user_id\": 4,\n\"money_transactions\": 1,\n\"pending_transactions\": 1,\n\"transactions_updates\": 3,\n\"help_tickets_updates\": 0,\n\"tickets_replies\": 2,\n\"account_login\": 0,\n\"change_password\": 1,\n\"verifications_setting\": 2\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification setting user ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "money_transactions",
            "description": "<p>Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pending_transactions",
            "description": "<p>pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "transactions_updates",
            "description": "<p>current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "help_tickets_updates",
            "description": "<p>help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tickets_replies",
            "description": "<p>new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "account_login",
            "description": "<p>login to user account(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "change_password",
            "description": "<p>change password(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "verifications_setting",
            "description": "<p>when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification setting unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification setting user ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "money_transactions",
            "description": "<p>Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pending_transactions",
            "description": "<p>pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "transactions_updates",
            "description": "<p>current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "help_tickets_updates",
            "description": "<p>help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tickets_replies",
            "description": "<p>new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "account_login",
            "description": "<p>login to user account(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "change_password",
            "description": "<p>change password(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "verifications_setting",
            "description": "<p>when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"id\": 4,\n  \"user_id\": 4,\n  \"money_transactions\": 1,\n  \"pending_transactions\": 1,\n  \"transactions_updates\": 3,\n  \"help_tickets_updates\": 0,\n  \"tickets_replies\": 2,\n  \"account_login\": 0,\n  \"change_password\": 1,\n  \"verifications_setting\": 2,\n  \"created_at\": \"2018-08-11T07:51:33.838Z\",\n  \"updated_at\": \"2018-08-11T07:51:33.838Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingUser",
            "description": "<p>User id has already been taken.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing User ID\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_settings_controller.rb",
    "groupTitle": "Notifications_Settings"
  },
  {
    "type": "put",
    "url": "/api/v1/notifications_settings/{:id}",
    "title": "3-Update an existing Notification Settings",
    "version": "0.3.0",
    "name": "PutNotificationSettings",
    "group": "Notifications_Settings",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/notifications_settings/3 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n\"money_transactions\": 2,\n\"pending_transactions\": 1,\n\"transactions_updates\": 3,\n\"help_tickets_updates\": 3,\n\"tickets_replies\": 2,\n\"account_login\": 1,\n\"change_password\": 0,\n\"verifications_setting\": 3\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "money_transactions",
            "description": "<p>Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pending_transactions",
            "description": "<p>pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "transactions_updates",
            "description": "<p>current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "help_tickets_updates",
            "description": "<p>help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tickets_replies",
            "description": "<p>new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "account_login",
            "description": "<p>login to user account(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "change_password",
            "description": "<p>change password(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "verifications_setting",
            "description": "<p>when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification setting unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification setting user ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "money_transactions",
            "description": "<p>Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pending_transactions",
            "description": "<p>pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "transactions_updates",
            "description": "<p>current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "help_tickets_updates",
            "description": "<p>help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tickets_replies",
            "description": "<p>new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "account_login",
            "description": "<p>login to user account(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "change_password",
            "description": "<p>change password(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "verifications_setting",
            "description": "<p>when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 3,\n  \"money_transactions\": 2,\n  \"pending_transactions\": 1,\n  \"transactions_updates\": 3,\n  \"help_tickets_updates\": 3,\n  \"tickets_replies\": 2,\n  \"account_login\": 1,\n  \"change_password\": 0,\n  \"verifications_setting\": 3,\n  \"user_id\": 3,\n  \"created_at\": \"2018-08-11T07:49:20.457Z\",\n  \"updated_at\": \"2018-08-11T08:20:57.508Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotificationSettingsNotFound",
            "description": "<p>The id of the Notification Settings was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"NotificationSettingsNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_settings_controller.rb",
    "groupTitle": "Notifications_Settings"
  },
  {
    "type": "get",
    "url": "/api/v1/add_defualt_notifications?id={:id}",
    "title": "4-Return to a defualt notification setting values",
    "version": "0.3.0",
    "name": "ReturnDefualtNotification",
    "group": "Notifications_Settings",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/add_defualt_notifications?id=4",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification setting unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>notification setting unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>notification setting user ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "money_transactions",
            "description": "<p>Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pending_transactions",
            "description": "<p>pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "transactions_updates",
            "description": "<p>current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "help_tickets_updates",
            "description": "<p>help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tickets_replies",
            "description": "<p>new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "account_login",
            "description": "<p>login to user account(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "change_password",
            "description": "<p>change password(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "verifications_setting",
            "description": "<p>when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail &amp; message).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"money_transactions\": 1,\n  \"pending_transactions\": 2,\n  \"transactions_updates\": 2,\n  \"help_tickets_updates\": 2,\n  \"tickets_replies\": 3,\n  \"account_login\": 3,\n  \"change_password\": 0,\n  \"verifications_setting\": 0,\n  \"id\": 4,\n  \"user_id\": 4,\n  \"created_at\": \"2018-08-11T07:51:33.838Z\",\n  \"updated_at\": \"2018-08-12T08:26:34.043Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotificationSettingNotFound",
            "description": "<p>The ID of this Notification Setting was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"This Notification Setting Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/notifications_settings_controller.rb",
    "groupTitle": "Notifications_Settings"
  },
  {
    "type": "get",
    "url": "/api/v1/pages",
    "title": "1-Request Pages List",
    "version": "0.3.0",
    "name": "GetPages",
    "group": "Pages",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/pages",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Page unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Page unique order.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>Page activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Page content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Page title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Page slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Page description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>Page keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "admin_id",
            "description": "<p>The admin unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"order\": 1,\n      \"active\": 1,\n      \"content\": \"the thnem adrny jahhs kjhsdn kkfiuds thrnsdo therrc vert.\",\n      \"title\": \"thern mandhtic\",\n      \"slug\": \"wesdm parny\",\n      \"description\": \"tingl man frrid mast blest mfjedre\",\n      \"keywords\": \"bleed masrt\",\n      \"admin_id\": 1,\n      \"created_at\": \"2018-09-01T11:46:21.515Z\",\n      \"updated_at\": \"2018-09-01T11:46:21.515Z\"\n  },\n  {\n      \"id\": 2,\n      \"order\": 2,\n      \"active\": 1,\n      \"content\": \"the best way to secure user account on payers.\",\n      \"title\": \"public sec\",\n      \"slug\": \"security slug wesd right\",\n      \"description\": \"user secure rand number\",\n      \"keywords\": \"privt\",\n      \"admin_id\": 1,\n      \"created_at\": \"2018-09-01T12:07:27.592Z\",\n      \"updated_at\": \"2018-09-01T12:07:27.592Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/pages_controller.rb",
    "groupTitle": "Pages"
  },
  {
    "type": "get",
    "url": "/api/pages/{:id}",
    "title": "2-Request Specific Page",
    "version": "0.3.0",
    "name": "GetSpecificPage",
    "group": "Pages",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/pages/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Page unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Page unique order.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>Page activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Page content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Page title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Page slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Page description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>Page keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "admin_id",
            "description": "<p>The admin unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 1,\n      \"order\": 1,\n      \"active\": 1,\n      \"content\": \"the thnem adrny jahhs kjhsdn kkfiuds thrnsdo therrc vert.\",\n      \"title\": \"thern mandhtic\",\n      \"slug\": \"wesdm parny\",\n      \"description\": \"tingl man frrid mast blest mfjedre\",\n      \"keywords\": \"bleed masrt\",\n      \"admin_id\": 1,\n      \"created_at\": \"2018-09-01T11:46:21.515Z\",\n      \"updated_at\": \"2018-09-01T11:46:21.515Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "PageNotFound",
            "description": "<p>The id of this Page was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Page Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/pages_controller.rb",
    "groupTitle": "Pages"
  },
  {
    "type": "post",
    "url": "/api/v1/pages",
    "title": "3-Create a new Page",
    "version": "0.3.0",
    "name": "PostPage",
    "group": "Pages",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/pages \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n \"order\": 2,\n \"active\": 1,\n \"content\": \"the best way to secure user account on payers.\",\n \"title\": \"public sec\",\n \"slug\": \"security slug wesd right\",\n \"description\": \"user secure rand number\",\n \"keywords\": \"privt\",\n \"admin_id\": 1\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Page unique order.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>Page activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Page content.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Page title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Page slug.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Page description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>Page keywords.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "admin_id",
            "description": "<p>The admin unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Page unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Page unique order.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>Page activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Page content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Page title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Page slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Page description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>Page keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "admin_id",
            "description": "<p>The admin unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 2,\n  \"order\": 2,\n  \"active\": 1,\n  \"content\": \"the best way to secure user account on payers.\",\n  \"title\": \"public sec\",\n  \"slug\": \"security slug wesd right\",\n  \"description\": \"user secure rand number\",\n  \"keywords\": \"privt\",\n  \"admin_id\": 1,\n  \"created_at\": \"2018-09-01T12:07:27.592Z\",\n  \"updated_at\": \"2018-09-01T12:07:27.592Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOrder",
            "description": "<p>this order has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 Existing Order\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/pages_controller.rb",
    "groupTitle": "Pages"
  },
  {
    "type": "put",
    "url": "/api/v1/pages/{:id}",
    "title": "4-Update an existing Page",
    "version": "0.3.0",
    "name": "PutPage",
    "group": "Pages",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/pages/3 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n \"active\": 0,\n \"content\": \"the best way to secure user account on payers.\",\n \"title\": \"public sec\",\n \"slug\": \"security slug wesd right\",\n \"description\": \"user secure rand number\",\n \"keywords\": \"privt\",\n \"admin_id\": 1\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Page unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>Page activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Page content.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Page title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Page slug.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Page description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>Page keywords.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Page unique order.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>Page activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Page content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Page title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Page slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Page description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>Page keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "admin_id",
            "description": "<p>The admin unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 2,\n  \"order\": 2,\n  \"active\": 1,\n  \"content\": \"the best way to secure user account on payers.\",\n  \"title\": \"public sec\",\n  \"slug\": \"security slug wesd right\",\n  \"description\": \"user secure rand number\",\n  \"keywords\": \"privt\",\n  \"admin_id\": 1,\n  \"created_at\": \"2018-09-01T12:07:27.592Z\",\n  \"updated_at\": \"2018-09-01T12:07:27.592Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "PageNotFound",
            "description": "<p>The id of this Page was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Page Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/pages_controller.rb",
    "groupTitle": "Pages"
  },
  {
    "type": "get",
    "url": "/api/v1/sms_logs",
    "title": "1-Request SMS Logs List",
    "version": "0.3.0",
    "name": "GetSMSLogs",
    "group": "SMS_Logs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/sms_logs",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>The receiver user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pinid",
            "description": "<p>SMS return pinId that was coming from the SMS provider.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sms_type",
            "description": "<p>SMS type (send_pin,verify_code,text_sms).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>SMS status(true = Verified , false = Not Verified).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date Sent.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n {\n    \"id\": 1,\n    \"user_id\": 1,\n    \"pinid\": \"43E94670D7190DBA62CA83492F56F365\",\n    \"sms_type\": 1,\n    \"status\": false,\n    \"created_at\": \"2018-08-25T12:49:45.190Z\",\n    \"updated_at\": \"2018-08-25T12:49:45.190Z\"\n },\n {\n    \"id\": 2,\n    \"user_id\": 1,\n    \"pinid\": \"A3D4C9447DFDD6F92332DDFE46A22367\",\n    \"sms_type\": 1,\n    \"status\": true,\n    \"created_at\": \"2018-08-25T12:52:27.287Z\",\n    \"updated_at\": \"2018-08-25T12:52:34.378Z\"\n }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/sms_logs_controller.rb",
    "groupTitle": "SMS_Logs"
  },
  {
    "type": "get",
    "url": "/api/translations/{:id}",
    "title": "2-Request Specific translation",
    "version": "0.3.0",
    "name": "GetSpecificTranslation",
    "group": "Translations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/translations/5",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>translation ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 5,\n        \"text\": \"test5\",\n        \"arabic_translation\": \"اختبار\",\n        \"english_translation\": \"test\",\n        \"created_at\": \"2018-07-19T11:02:37.000Z\",\n        \"updated_at\": \"2018-07-19T11:02:37.000Z\",             \n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TranslationNotFound",
            "description": "<p>The id of the Translation was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TranslationNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "get",
    "url": "/api/translations",
    "title": "1-Request translations List",
    "version": "0.3.0",
    "name": "GetTranslation",
    "group": "Translations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/translations",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>translation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n    {\n        \"id\": 1,\n        \"text\": \"payers\",\n        \"arabic_translation\": \"بايرز\",\n        \"english_translation\": \"payers\",\n        \"created_at\": \"2018-07-19T11:02:30.110Z\",\n        \"updated_at\": \"2018-07-19T11:02:30.110Z\",             \n    },\n    {\n        \"id\": 2,\n        \"text\": \"welcome\",\n        \"arabic_translation\": \"مرحبا\",\n        \"english_translation\": \"welcome\",\n        \"created_at\": \"2018-07-19T11:02:37.000Z\",\n        \"updated_at\": \"2018-07-19T11:02:37.000Z\",             \n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "post",
    "url": "/api/translations",
    "title": "3-Create a new translation",
    "version": "0.3.0",
    "name": "PostTranslation",
    "group": "Translations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>new translation ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 4,\n        \"text\": \"ahmad\",\n        \"arabic_translation\": \"احمد حسن\",\n        \"english_translation\": \"ahmad hassan\",\n        \"created_at\": \"2018-07-19T15:09:07.000Z\",\n        \"updated_at\": \"2018-07-19T15:09:07.000Z\",             \n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TranslationNotFound",
            "description": "<p>The id of the Translation was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TranslationNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "put",
    "url": "/api/translations/{:id}",
    "title": "4-Change an existing translation",
    "version": "0.3.0",
    "name": "PutTranslation",
    "group": "Translations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>translation ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 4,\n        \"text\": \"ahmad\",\n        \"arabic_translation\": \"احمد\",\n        \"english_translation\": \"ahmad\",\n        \"created_at\": \"2018-07-19T15:09:07.000Z\",\n        \"updated_at\": \"2018-07-19T15:21:31.000Z\",             \n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "get",
    "url": "/api/user_wallets/{:id}",
    "title": "2-Request Specific User Wallet",
    "version": "0.3.0",
    "name": "GetSpecificUserWallet",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_wallets/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card Operation ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 1,\n      \"currency\": \"USD\",\n      \"amount\": 1000,\n      \"user_id\": 1,\n      \"status\": 1,\n      \"uuid\": \"0fa993bcfa59\",\n      \"created_at\": \"2018-08-02T11:03:14.634Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.183Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WalletNotFound",
            "description": "<p>The id of this User Wallet was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Wallet Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/user_wallets",
    "title": "1-Request Users Wallets List",
    "version": "0.3.0",
    "name": "GetUserWallets",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_wallets",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"currency\": \"USD\",\n      \"amount\": 1000,\n      \"user_id\": 1,\n      \"status\": 1,\n      \"uuid\": \"0fa993bcfa59\",\n      \"created_at\": \"2018-08-02T11:03:14.634Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.183Z\"\n  },\n  {\n      \"id\": 2,\n      \"currency\": \"USD\",\n      \"amount\": 500,\n      \"user_id\": 2,\n      \"status\": 1,\n      \"uuid\": \"0b51db6a5988\",\n      \"created_at\": \"2018-08-02T11:03:52.375Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.179Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "post",
    "url": "/api/v1/user_wallets",
    "title": "3-Create a new User Wallet",
    "version": "0.3.0",
    "name": "PostUserWallet",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/user_wallets \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n\"amount\": \"500\",\n\"user_id\": 3,\n\"status\": 1\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID (unique).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 3,\n      \"currency\": \"USD\",\n      \"amount\": 500,\n      \"user_id\": 3,\n      \"status\": 1,\n      \"uuid\": \"0fa663bcsa47\",\n      \"created_at\": \"2018-08-02T15:03:04.333Z\",\n      \"updated_at\": \"2018-08-02T15:03:04.383Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingUserId",
            "description": "<p>User Id has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 Existing User Id\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/send_sms?tel={phone_number}",
    "title": "6-Send Verification SMS Code to Users",
    "version": "0.3.0",
    "name": "SendSmsCode",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/send_sms?tel=20106455****",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tel",
            "description": "<p>User phone number + country code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tel",
            "description": "<p>User phone number.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \" Code was send successfully to number 201064****** \"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "SendCodeFailed",
            "description": "<p>Something went wrong while sending SMS code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"Something wrong, Sending code was failed\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/transfer_balance?=&userfrom={:sender_id}&userto={:receiver_id}&transferamount={:amount}",
    "title": "5-Transfer Balances Between Users",
    "version": "0.3.0",
    "name": "TransferBalance",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/transfer_balance?=&userfrom=1&userto=2&transferamount=10",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userfrom",
            "description": "<p>the sender user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userto",
            "description": "<p>the receiver user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "transferamount",
            "description": "<p>the amount transferred between users.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"success\": \"Balance was successfully transferred\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "{\n  \"error\": \"You don't have enough money for this operation\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "{\n  \"error\": \"Sorry, The transferd balance must be greater than 2 USD\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "{\n  \"error\": \"Sorry, The transferd balance must be less or equal than 10 USD\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response4:",
          "content": "{\n  \"error\": \"Sorry, Your Wallet was Disabled\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response5:",
          "content": "{\n  \"error\": \"The sender User Wasn't Stored in Our Database\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response6:",
          "content": "{\n  \"error\": \"The receiver User Wasn't Stored in Our Database\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response6:",
          "content": "{\n  \"error\": \"Sorry, The Receiver User Wallet was Disabled\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response8:",
          "content": "{\n  \"error\": \"You can't transfer balance to yourself\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/wallets_status?id={:id}",
    "title": "4-Update a Specific User Wallet Status",
    "version": "0.3.0",
    "name": "UpdateWalletsStatus",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/wallets_status?id=2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 2,\n      \"currency\": \"USD\",\n      \"amount\": 500,\n      \"user_id\": 2,\n      \"status\": 0,\n      \"uuid\": \"0b51db6a5988\",\n      \"created_at\": \"2018-08-02T11:03:52.375Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.179Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WalletNotFound",
            "description": "<p>The id of this User Wallet was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"User Wallet Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/verifysms?code={SMS_code}",
    "title": "7-Verify SMS Code",
    "version": "0.3.0",
    "name": "VerifySMSCode",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/verifysms?code=3660",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>The SMS verification code.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \"Valid code, successfully activated\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>The verification code was wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"Wrong code Try again later\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  }
] });
