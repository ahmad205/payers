class CreateWalletsTransferRatios < ActiveRecord::Migration[5.2]
  def change
    create_table :wallets_transfer_ratios do |t|
      t.string :key
      t.float :value , :limit=>53
      t.string :description

      t.timestamps
    end
  end
end
