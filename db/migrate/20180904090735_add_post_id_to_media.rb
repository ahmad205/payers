class AddPostIdToMedia < ActiveRecord::Migration[5.2]
  def change
    add_column :media, :post_id, :string
  end
end
