class CreateSmsLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :sms_logs do |t|
      t.integer :user_id
      t.string :pinid
      t.integer :sms_type
      t.boolean :status , :default => false

      t.timestamps
    end
  end
end
