# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_17_141345) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.integer "user_id"
    t.string "address"
    t.string "country"
    t.string "governorate"
    t.string "city"
    t.string "street"
    t.boolean "default_address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards", force: :cascade do |t|
    t.string "number"
    t.integer "value"
    t.integer "status"
    t.date "expired_at"
    t.integer "user_id"
    t.integer "invoice_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards_categories", force: :cascade do |t|
    t.integer "card_value"
    t.integer "active"
    t.integer "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards_logs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "action_type"
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "card_id"
  end

  create_table "media", force: :cascade do |t|
    t.integer "admin_id"
    t.string "media_type"
    t.string "post_content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "post_id"
  end

  create_table "money_ops", force: :cascade do |t|
    t.string "opid"
    t.integer "optype"
    t.float "amount", limit: 53
    t.integer "payment_gateway"
    t.integer "status"
    t.datetime "payment_date"
    t.integer "payment_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "user_id"
    t.string "title"
    t.string "description"
    t.integer "notification_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications_settings", force: :cascade do |t|
    t.integer "user_id"
    t.integer "money_transactions"
    t.integer "pending_transactions"
    t.integer "transactions_updates"
    t.integer "help_tickets_updates"
    t.integer "tickets_replies"
    t.integer "account_login"
    t.integer "change_password"
    t.integer "verifications_setting"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", force: :cascade do |t|
    t.integer "order"
    t.integer "active"
    t.string "content"
    t.string "title"
    t.string "slug"
    t.string "description"
    t.string "keywords"
    t.integer "admin_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sms_logs", force: :cascade do |t|
    t.integer "user_id"
    t.string "pinid"
    t.integer "sms_type"
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "translations", force: :cascade do |t|
    t.string "text"
    t.string "arabic_translation"
    t.string "english_translation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "controller"
  end

  create_table "uploads", force: :cascade do |t|
    t.text "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_wallets", force: :cascade do |t|
    t.string "currency"
    t.float "amount", limit: 53
    t.integer "user_id"
    t.integer "status"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wallets_transfer_ratios", force: :cascade do |t|
    t.string "key"
    t.float "value", limit: 53
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
