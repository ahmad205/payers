require 'rails_helper'

RSpec.describe "notifications_settings/index", type: :view do
  before(:each) do
    assign(:notifications_settings, [
      NotificationsSetting.create!(
        :user_id => 2,
        :money_transactions => 3,
        :pending_transactions => 4,
        :transactions_updates => 5,
        :help_tickets_updates => 6,
        :tickets_replies => 7,
        :account_login => 8,
        :change_password => 9,
        :verifications_setting => 10
      ),
      NotificationsSetting.create!(
        :user_id => 2,
        :money_transactions => 3,
        :pending_transactions => 4,
        :transactions_updates => 5,
        :help_tickets_updates => 6,
        :tickets_replies => 7,
        :account_login => 8,
        :change_password => 9,
        :verifications_setting => 10
      )
    ])
  end

  it "renders a list of notifications_settings" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
    assert_select "tr>td", :text => 9.to_s, :count => 2
    assert_select "tr>td", :text => 10.to_s, :count => 2
  end
end
