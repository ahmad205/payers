require 'rails_helper'

RSpec.describe "money_ops/new", type: :view do
  before(:each) do
    assign(:money_op, MoneyOp.new(
      :opid => "MyString",
      :optype => 1,
      :amount => "MyString",
      :payment_gateway => 1,
      :status => 1,
      :payment_id => 1,
      :user_id => 1
    ))
  end

  it "renders new money_op form" do
    render

    assert_select "form[action=?][method=?]", money_ops_path, "post" do

      assert_select "input[name=?]", "money_op[opid]"

      assert_select "input[name=?]", "money_op[optype]"

      assert_select "input[name=?]", "money_op[amount]"

      assert_select "input[name=?]", "money_op[payment_gateway]"

      assert_select "input[name=?]", "money_op[status]"

      assert_select "input[name=?]", "money_op[payment_id]"

      assert_select "input[name=?]", "money_op[user_id]"
    end
  end
end
