require 'rails_helper'

RSpec.describe "wallets_transfer_ratios/edit", type: :view do
  before(:each) do
    @wallets_transfer_ratio = assign(:wallets_transfer_ratio, WalletsTransferRatio.create!(
      :key => "MyString",
      :value => 1.5,
      :description => "MyString"
    ))
  end

  it "renders the edit wallets_transfer_ratio form" do
    render

    assert_select "form[action=?][method=?]", wallets_transfer_ratio_path(@wallets_transfer_ratio), "post" do

      assert_select "input[name=?]", "wallets_transfer_ratio[key]"

      assert_select "input[name=?]", "wallets_transfer_ratio[value]"

      assert_select "input[name=?]", "wallets_transfer_ratio[description]"
    end
  end
end
