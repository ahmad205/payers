require 'rails_helper'

RSpec.describe "wallets_transfer_ratios/new", type: :view do
  before(:each) do
    assign(:wallets_transfer_ratio, WalletsTransferRatio.new(
      :key => "MyString",
      :value => 1.5,
      :description => "MyString"
    ))
  end

  it "renders new wallets_transfer_ratio form" do
    render

    assert_select "form[action=?][method=?]", wallets_transfer_ratios_path, "post" do

      assert_select "input[name=?]", "wallets_transfer_ratio[key]"

      assert_select "input[name=?]", "wallets_transfer_ratio[value]"

      assert_select "input[name=?]", "wallets_transfer_ratio[description]"
    end
  end
end
