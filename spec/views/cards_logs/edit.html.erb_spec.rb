require 'rails_helper'

RSpec.describe "cards_logs/edit", type: :view do
  before(:each) do
    @cards_log = assign(:cards_log, CardsLog.create!(
      :user_id => 1,
      :action_type => 1,
      :ip => "MyString"
    ))
  end

  it "renders the edit cards_log form" do
    render

    assert_select "form[action=?][method=?]", cards_log_path(@cards_log), "post" do

      assert_select "input[name=?]", "cards_log[user_id]"

      assert_select "input[name=?]", "cards_log[action_type]"

      assert_select "input[name=?]", "cards_log[ip]"
    end
  end
end
