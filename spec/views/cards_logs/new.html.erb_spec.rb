require 'rails_helper'

RSpec.describe "cards_logs/new", type: :view do
  before(:each) do
    assign(:cards_log, CardsLog.new(
      :user_id => 1,
      :action_type => 1,
      :ip => "MyString"
    ))
  end

  it "renders new cards_log form" do
    render

    assert_select "form[action=?][method=?]", cards_logs_path, "post" do

      assert_select "input[name=?]", "cards_log[user_id]"

      assert_select "input[name=?]", "cards_log[action_type]"

      assert_select "input[name=?]", "cards_log[ip]"
    end
  end
end
