require 'rails_helper'

RSpec.describe "user_wallets/index", type: :view do
  before(:each) do
    assign(:user_wallets, [
      UserWallet.create!(
        :currency => "Currency",
        :amount => "Amount",
        :user_id => 2,
        :status => 3,
        :uuid => "Uuid"
      ),
      UserWallet.create!(
        :currency => "Currency",
        :amount => "Amount",
        :user_id => 2,
        :status => 3,
        :uuid => "Uuid"
      )
    ])
  end

  it "renders a list of user_wallets" do
    render
    assert_select "tr>td", :text => "Currency".to_s, :count => 2
    assert_select "tr>td", :text => "Amount".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Uuid".to_s, :count => 2
  end
end
