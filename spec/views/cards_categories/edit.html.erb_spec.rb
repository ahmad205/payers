require 'rails_helper'

RSpec.describe "cards_categories/edit", type: :view do
  before(:each) do
    @cards_category = assign(:cards_category, CardsCategory.create!(
      :card_value => 1,
      :active => 1,
      :order => 1
    ))
  end

  it "renders the edit cards_category form" do
    render

    assert_select "form[action=?][method=?]", cards_category_path(@cards_category), "post" do

      assert_select "input[name=?]", "cards_category[card_value]"

      assert_select "input[name=?]", "cards_category[active]"

      assert_select "input[name=?]", "cards_category[order]"
    end
  end
end
