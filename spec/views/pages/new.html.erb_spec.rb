require 'rails_helper'

RSpec.describe "pages/new", type: :view do
  before(:each) do
    assign(:page, Page.new(
      :order => 1,
      :active => 1,
      :content => "MyString",
      :title => "MyString",
      :slug => "MyString",
      :description => "MyString",
      :keywords => "MyString",
      :admin_id => 1
    ))
  end

  it "renders new page form" do
    render

    assert_select "form[action=?][method=?]", pages_path, "post" do

      assert_select "input[name=?]", "page[order]"

      assert_select "input[name=?]", "page[active]"

      assert_select "input[name=?]", "page[content]"

      assert_select "input[name=?]", "page[title]"

      assert_select "input[name=?]", "page[slug]"

      assert_select "input[name=?]", "page[description]"

      assert_select "input[name=?]", "page[keywords]"

      assert_select "input[name=?]", "page[admin_id]"
    end
  end
end
