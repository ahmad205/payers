require 'rails_helper'

RSpec.describe "notifications/edit", type: :view do
  before(:each) do
    @notification = assign(:notification, Notification.create!(
      :user_id => 1,
      :title => "MyString",
      :description => "MyString",
      :notification_type => 1
    ))
  end

  it "renders the edit notification form" do
    render

    assert_select "form[action=?][method=?]", notification_path(@notification), "post" do

      assert_select "input[name=?]", "notification[user_id]"

      assert_select "input[name=?]", "notification[title]"

      assert_select "input[name=?]", "notification[description]"

      assert_select "input[name=?]", "notification[notification_type]"
    end
  end
end
