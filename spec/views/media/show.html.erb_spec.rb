require 'rails_helper'

RSpec.describe "media/show", type: :view do
  before(:each) do
    @medium = assign(:medium, Medium.create!(
      :admin_id => 2,
      :media_type => "Media Type",
      :post_content => "Post Content"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Media Type/)
    expect(rendered).to match(/Post Content/)
  end
end
