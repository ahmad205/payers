require 'rails_helper'

RSpec.describe "addresses/show", type: :view do
  before(:each) do
    @address = assign(:address, Address.create!(
      :user_id => 2,
      :address => "Address",
      :country => "Country",
      :governorate => "Governorate",
      :city => "City",
      :street => "Street",
      :default_address => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Governorate/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Street/)
    expect(rendered).to match(/false/)
  end
end
