require 'rails_helper'

RSpec.describe "addresses/index", type: :view do
  before(:each) do
    assign(:addresses, [
      Address.create!(
        :user_id => 2,
        :address => "Address",
        :country => "Country",
        :governorate => "Governorate",
        :city => "City",
        :street => "Street",
        :default_address => false
      ),
      Address.create!(
        :user_id => 2,
        :address => "Address",
        :country => "Country",
        :governorate => "Governorate",
        :city => "City",
        :street => "Street",
        :default_address => false
      )
    ])
  end

  it "renders a list of addresses" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Governorate".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Street".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
