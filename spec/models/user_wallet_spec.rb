require 'rails_helper'

# RSpec.describe UserWallet, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
# end

describe UserWallet, '#transferbalance' do
 
  it 'returns the balance of the sender and receiver' do
  # Setup    
    @transfer_amount = 5
    @user_from = 5
    @user_to = 6
    @new_wallet_send = UserWallet.create(amount: 200, user_id: @user_from , status: 1)
    @new_wallet_receive = UserWallet.create( amount: 100, user_id: @user_to , status: 1)
    @transfer_check_message = UserWallet.checktransfer(@user_from,@user_to,@transfer_amount)

  # Exercise  
   @sender_new_balance = @new_wallet_send.amount.to_f - @transfer_amount.to_f
   @receiver_new_balance = @new_wallet_receive.amount.to_f + @transfer_amount.to_f
   @new_wallet_send.update(:amount => @sender_new_balance)
   @new_wallet_receive.update(:amount => @receiver_new_balance)
   @sender_balance2 = UserWallet.where(:user_id => @user_from).pluck(:amount).first
   @receiver_balance2 = UserWallet.where(:user_id => @user_to).pluck(:amount).first
   
  # Verify
    expect(@transfer_check_message).to eq ""
    expect(@sender_balance2).to eq @sender_new_balance
    expect(@receiver_balance2).to eq @receiver_new_balance

  # Teardown is for now mostly handled by RSpec itself
  end
 
end
