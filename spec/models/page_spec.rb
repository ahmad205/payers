require 'rails_helper'


describe Page, '#createnewpage' do
 
  it 'returns a new page title' do
  # Setup    
    @new_page = Page.create(order: 3,active: 1,content: 'the best way to trnsfer balances',title: 'balance exchange',slug: 'money trans',
    description: 'transfer money between users',keywords: 'Usage_Policy',admin_id: 1)

  # Exercise  
   @updated_page = @new_page.update(:title => 'money transfer')
   
  # Verify
    expect(@new_page.title).to eq 'money transfer'

  # Teardown is for now mostly handled by RSpec itself
  end
 
end

describe Page,"DELETE /pages/:id" do

  it 'should delete the page' do
  # Setup 
    @new_page = Page.create(order: 3,active: 1,content: 'the best way to trnsfer balances',title: 'balance exchange',slug: 'money trans',
    description: 'transfer money between users',keywords: 'Usage_Policy',admin_id: 1)

  # Exercise
    @new_page.destroy
    @pages_after_delete = Page.where(:order => 3).first

  # Verify
    expect(@pages_after_delete).to eq nil
  end

end
