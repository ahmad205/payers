require 'rails_helper'

RSpec.describe Card, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  describe Card do
  it { should validate_uniqueness_of(:number) }
  it { should validate_length_of(:number), is: 16 }
  end
end
