require "rails_helper"

RSpec.describe WalletsTransferRatiosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/wallets_transfer_ratios").to route_to("wallets_transfer_ratios#index")
    end

    it "routes to #new" do
      expect(:get => "/wallets_transfer_ratios/new").to route_to("wallets_transfer_ratios#new")
    end

    it "routes to #show" do
      expect(:get => "/wallets_transfer_ratios/1").to route_to("wallets_transfer_ratios#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/wallets_transfer_ratios/1/edit").to route_to("wallets_transfer_ratios#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/wallets_transfer_ratios").to route_to("wallets_transfer_ratios#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/wallets_transfer_ratios/1").to route_to("wallets_transfer_ratios#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/wallets_transfer_ratios/1").to route_to("wallets_transfer_ratios#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/wallets_transfer_ratios/1").to route_to("wallets_transfer_ratios#destroy", :id => "1")
    end

  end
end
