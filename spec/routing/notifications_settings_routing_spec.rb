require "rails_helper"

RSpec.describe NotificationsSettingsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/notifications_settings").to route_to("notifications_settings#index")
    end

    it "routes to #new" do
      expect(:get => "/notifications_settings/new").to route_to("notifications_settings#new")
    end

    it "routes to #show" do
      expect(:get => "/notifications_settings/1").to route_to("notifications_settings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/notifications_settings/1/edit").to route_to("notifications_settings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/notifications_settings").to route_to("notifications_settings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/notifications_settings/1").to route_to("notifications_settings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/notifications_settings/1").to route_to("notifications_settings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/notifications_settings/1").to route_to("notifications_settings#destroy", :id => "1")
    end

  end
end
