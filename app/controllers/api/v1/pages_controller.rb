module Api
  module V1
    class PagesController < ApplicationController
      before_action :set_page, only: [:show, :edit, :update]
      skip_before_action :verify_authenticity_token


      # GET /pages.json
      def index

        # =begin
        # @api {get} /api/v1/pages 1-Request Pages List
        # @apiVersion 0.3.0
        # @apiName GetPages
        # @apiGroup Pages
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/pages
        # @apiSuccess {Number} id Page unique ID.
        # @apiSuccess {Number} order Page unique order.
        # @apiSuccess {Number} active Page activity (1 for enable , 0 for disable).
        # @apiSuccess {String} content Page content.
        # @apiSuccess {String} title Page title.
        # @apiSuccess {String} slug Page slug.
        # @apiSuccess {String} description Page description.
        # @apiSuccess {String} keywords Page keywords.
        # @apiSuccess {Number} admin_id The admin unique id.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "order": 1,
        #       "active": 1,
        #       "content": "the thnem adrny jahhs kjhsdn kkfiuds thrnsdo therrc vert.",
        #       "title": "thern mandhtic",
        #       "slug": "wesdm parny",
        #       "description": "tingl man frrid mast blest mfjedre",
        #       "keywords": "bleed masrt",
        #       "admin_id": 1,
        #       "created_at": "2018-09-01T11:46:21.515Z",
        #       "updated_at": "2018-09-01T11:46:21.515Z"
        #   },
        #   {
        #       "id": 2,
        #       "order": 2,
        #       "active": 1,
        #       "content": "the best way to secure user account on payers.",
        #       "title": "public sec",
        #       "slug": "security slug wesd right",
        #       "description": "user secure rand number",
        #       "keywords": "privt",
        #       "admin_id": 1,
        #       "created_at": "2018-09-01T12:07:27.592Z",
        #       "updated_at": "2018-09-01T12:07:27.592Z"
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        @pages = Page.all
        respond_to do |format|
          format.json { render json: @pages }
        end
        

      end

      # GET /pages/1.json
      def show

        # =begin
        # @api {get} /api/pages/{:id} 2-Request Specific Page
        # @apiVersion 0.3.0
        # @apiName GetSpecificPage
        # @apiGroup Pages
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/pages/1
        # @apiParam {Number} id Page unique ID.
        # @apiSuccess {Number} order Page unique order.
        # @apiSuccess {Number} active Page activity (1 for enable , 0 for disable).
        # @apiSuccess {String} content Page content.
        # @apiSuccess {String} title Page title.
        # @apiSuccess {String} slug Page slug.
        # @apiSuccess {String} description Page description.
        # @apiSuccess {String} keywords Page keywords.
        # @apiSuccess {Number} admin_id The admin unique id.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #       "id": 1,
        #       "order": 1,
        #       "active": 1,
        #       "content": "the thnem adrny jahhs kjhsdn kkfiuds thrnsdo therrc vert.",
        #       "title": "thern mandhtic",
        #       "slug": "wesdm parny",
        #       "description": "tingl man frrid mast blest mfjedre",
        #       "keywords": "bleed masrt",
        #       "admin_id": 1,
        #       "created_at": "2018-09-01T11:46:21.515Z",
        #       "updated_at": "2018-09-01T11:46:21.515Z"
        #   }
        # @apiError PageNotFound The id of this Page was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Page Not Found"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
        flash[:error]= "Page Not Found"
        respond_to do |format|
          if @page
            format.json { render json: @page }
          else
            format.json { render json: flash }
          end
        end
      end

      # GET /pages/new
      def new
        @page = Page.new
      end

      # GET /pages/1/edit
      def edit
      end

      # POST /pages.json
      def create

        # =begin
        # @api {post} /api/v1/pages 3-Create a new Page
        # @apiVersion 0.3.0
        # @apiName PostPage
        # @apiGroup Pages
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/pages \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #  "order": 2,
        #  "active": 1,
        #  "content": "the best way to secure user account on payers.",
        #  "title": "public sec",
        #  "slug": "security slug wesd right",
        #  "description": "user secure rand number",
        #  "keywords": "privt",
        #  "admin_id": 1
        # }'
        # @apiParam {Number} order Page unique order.
        # @apiParam {Number} active Page activity (1 for enable , 0 for disable).
        # @apiParam {String} content Page content.
        # @apiParam {String} title Page title.
        # @apiParam {String} slug Page slug.
        # @apiParam {String} description Page description.
        # @apiParam {String} keywords Page keywords.
        # @apiParam {Number} admin_id The admin unique id.
        # @apiSuccess {Number} id Page unique ID.
        # @apiSuccess {Number} order Page unique order.
        # @apiSuccess {Number} active Page activity (1 for enable , 0 for disable).
        # @apiSuccess {String} content Page content.
        # @apiSuccess {String} title Page title.
        # @apiSuccess {String} slug Page slug.
        # @apiSuccess {String} description Page description.
        # @apiSuccess {String} keywords Page keywords.
        # @apiSuccess {Number} admin_id The admin unique id.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 2,
        #   "order": 2,
        #   "active": 1,
        #   "content": "the best way to secure user account on payers.",
        #   "title": "public sec",
        #   "slug": "security slug wesd right",
        #   "description": "user secure rand number",
        #   "keywords": "privt",
        #   "admin_id": 1,
        #   "created_at": "2018-09-01T12:07:27.592Z",
        #   "updated_at": "2018-09-01T12:07:27.592Z"
        # }
        # @apiError ExistingOrder this order has already been taken.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 Existing Order
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError MissingToken invalid token .
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        @page = Page.new(page_params)

        respond_to do |format|
          if @page.save
            format.json { render json: @page, status: :created, location: @page }
          else
            format.json { render json: @page.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /pages/1.json
      def update

        # =begin
        # @api {put} /api/v1/pages/{:id} 4-Update an existing Page
        # @apiVersion 0.3.0
        # @apiName PutPage
        # @apiGroup Pages
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/v1/pages/3 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #  "active": 0,
        #  "content": "the best way to secure user account on payers.",
        #  "title": "public sec",
        #  "slug": "security slug wesd right",
        #  "description": "user secure rand number",
        #  "keywords": "privt",
        #  "admin_id": 1
        # }'
        # @apiParam {Number} id Page unique ID.
        # @apiParam {Number} active Page activity (1 for enable , 0 for disable).
        # @apiParam {String} content Page content.
        # @apiParam {String} title Page title.
        # @apiParam {String} slug Page slug.
        # @apiParam {String} description Page description.
        # @apiParam {String} keywords Page keywords.
        # @apiSuccess {Number} order Page unique order.
        # @apiSuccess {Number} active Page activity (1 for enable , 0 for disable).
        # @apiSuccess {String} content Page content.
        # @apiSuccess {String} title Page title.
        # @apiSuccess {String} slug Page slug.
        # @apiSuccess {String} description Page description.
        # @apiSuccess {String} keywords Page keywords.
        # @apiSuccess {Number} admin_id The admin unique id.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 2,
        #   "order": 2,
        #   "active": 1,
        #   "content": "the best way to secure user account on payers.",
        #   "title": "public sec",
        #   "slug": "security slug wesd right",
        #   "description": "user secure rand number",
        #   "keywords": "privt",
        #   "admin_id": 1,
        #   "created_at": "2018-09-01T12:07:27.592Z",
        #   "updated_at": "2018-09-01T12:07:27.592Z"
        # }
        # @apiError PageNotFound The id of this Page was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Page Not Found"
        #   }
        # @apiError MissingToken invalid token .
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        respond_to do |format|
          if @page.update(page_params)
            format.json { render json: @page, status: :ok, location: @page }
          else
            format.json { render json: @page.errors, status: :unprocessable_entity }
          end
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_page
          @page = Page.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def page_params
          params.require(:page).permit(:order, :active, :content, :title, :slug, :description, :keywords, :admin_id)
        end
    end
  end
end
