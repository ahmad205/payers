class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]

  # GET list of pages and display it
  # @return [id] Page unique ID.
  # @return [order] Page unique order.
  # @return [active] Page activity (1 for enable , 0 for disable).
  # @return [content] Page content.
  # @return [title] Page title.
  # @return [slug] Page slug.
  # @return [description] Page description.
  # @return [keywords] Page keywords.
  # @return [admin_id] The admin unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @pages = Page.all
  end

  # GET a Specific existing page
  # @param [Integer] id Page unique ID.
  # @return [order] Page unique order.
  # @return [active] Page activity (1 for enable , 0 for disable).
  # @return [content] Page content.
  # @return [title] Page title.
  # @return [slug] Page slug.
  # @return [description] Page description.
  # @return [keywords] Page keywords.
  # @return [admin_id] The admin unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new page
  # @param [Integer] id Page unique ID.
  # @param [Integer] order Page unique order.
  # @param [Integer] active Page activity (1 for enable , 0 for disable).
  # @param [String] content Page content.
  # @param [String] title Page title.
  # @param [String] slug Page slug.
  # @param [String] description Page description.
  # @param [String] keywords Page keywords.
  # @param [Integer] admin_id The admin unique id.
  def new
    @page = Page.new
  end

  # GET an existing page to edit params
  # @param [Integer] active Page activity (1 for enable , 0 for disable).
  # @param [String] content Page content.
  # @param [String] title Page title.
  # @param [String] slug Page slug.
  # @param [String] description Page description.
  # @param [String] keywords Page keywords.
  def edit
  end

  # POST a new page and save it
  # @return [id] Page unique ID.
  # @return [order] Page unique order.
  # @return [active] Page activity (1 for enable , 0 for disable).
  # @return [content] Page content.
  # @return [title] Page title.
  # @return [slug] Page slug.
  # @return [description] Page description.
  # @return [keywords] Page keywords.
  # @return [admin_id] The admin unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing page params
  # @return [id] Page unique ID.
  # @return [order] Page unique order.
  # @return [active] Page activity (1 for enable , 0 for disable).
  # @return [content] Page content.
  # @return [title] Page title.
  # @return [slug] Page slug.
  # @return [description] Page description.
  # @return [keywords] Page keywords.
  # @return [admin_id] The admin unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def sitemap
    @base_url = "http://#{request.host_with_port}/"
    @pages = Page.all
    respond_to do |format|
      format.xml
    end
  end

  # DELETE an existing card
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'page was successfully destroyed.' }
      format.json { render json: @page, status: :ok }
    end
  end

  def facebook
  end

  def facebook_post

    require 'uri'
    require 'net/http'

    @message = params[:message]
    url = URI("https://graph.facebook.com/306574250124113/feed")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["authorization"] = 'AWS4-HMAC-SHA256 Credential=/20180903/us-east-1/execute-api/aws4_request, SignedHeaders=content-length;content-type;host;x-amz-date, Signature=ebb6abcd1f27bb711f04b29467df993ad3c83b39b2b9291f1707d81e997e9a96'
    request["content-type"] = 'application/json'
    request["host"] = 'graph.facebook.com'
    request["x-amz-date"] = '20180903T132044Z'
    request["cache-control"] = 'no-cache'
    request.body = JSON.dump({    
      "message": @message,
      "access_token": "EAAfCzkV8a8YBAEC8ZCCmVIWkb9QEgxbw2HL94m04iDZBf6RfG1YiHQLgYAaYkMeGWHJFxrbZAx7j2ORSxtcuZAZB5Nn3EckMnXDYBSUhL9ZBH7WFjp7MuaZChOfRy4nXsmTBXfvCNj8THyQ4QmqBqczvRC7ZBCYFmzrVioQG5kh8nXn6wISMGwuiEi0zkscng8wD674T7lV2bQZDZD",
      })
      response = http.request(request)
      puts response.read_body

      respond_to do |format|
        format.html { redirect_to pages_url, notice: 'Post successfully on Facebook.' }
      end  

  end

  def change_locale
    I18n.locale = params[:locale] 
    redirect_to(request.env['HTTP_REFERER']) 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:order, :active, :content, :title, :slug, :description, :keywords, :admin_id)
    end
end
