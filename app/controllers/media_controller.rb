class MediaController < ApplicationController
  before_action :set_media, only: [:destroy]

  # GET list of Media Services and display it
  # @return [id] media unique ID (Created automatically).
  # @return [admin_id] The admin id of the post creator.
  # @return [media_type] The media service name(facebook,twitter,...).
  # @return [post_content] The media post content.
  # @return [post_id] The media post id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @media = Media.all
  end

  # Create new post in facebook
  # @param [Integer] id media unique ID (Created automatically).
  # @param [Integer] admin_id The admin id of the post creator.
  # @param [String] media_type The media service name(facebook,twitter,...).
  # @param [String] post_content The media post content.
  # @param [Integer] post_id The media post id.
  def facebook
  end

  # Post a new content in facebook
  # @return [id] media unique ID (Created automatically).
  # @return [admin_id] The admin id of the post creator.
  # @return [media_type] The media service name(facebook,twitter,...).
  # @return [post_content] The media post content.
  # @return [post_id] The media post id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def facebook_post

    @message = params[:message]
    @response = FacebookService.new(message:@message).send_post
    respond_to do |format|
      if @response != nil
        format.html { redirect_to media_url, notice: 'Post successfully on Facebook.' }
      else 
        format.html { redirect_to media_url, notice: 'Sorry something wrong' }
      end
    end

  end

  # Delete an existing post in facebook
  # @param [Integer] id media unique ID (Created automatically).
  # @param [Integer] post_id The media post id.
  def destroy

    @post_id = @media.post_id
    @applications_list = FacebookService.new(post_id:@post_id).delete_post

    if @applications_list["success"] == true
      @media.destroy
      respond_to do |format|
        format.html { redirect_to media_url, notice: 'Post was successfully destroyed.' }
      end
    else
      respond_to do |format|
        format.html { redirect_to media_url, notice: 'Sorry something went wrong!' }
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_media
      @media = Media.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def media_params
      params.require(:media).permit(:admin_id, :media_type, :post_content, :post_id)
    end
end
