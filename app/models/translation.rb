class Translation < ApplicationRecord
    validates :controller, presence: true
    validates_uniqueness_of :text
end
