class Upload < ApplicationRecord
    has_one_attached :avatar

    def image_file=(avatar)
        self.image_file.attach(avatar)
    end 

    
end

