json.extract! translation, :id, :text, :arabic_translation, :english_translation, :created_at, :updated_at
json.url translation_url(translation, format: :json)
