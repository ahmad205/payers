json.extract! medium, :id, :admin_id, :media_type, :post_content, :created_at, :updated_at
json.url medium_url(medium, format: :json)
