json.extract! page, :id, :order, :active, :content, :title, :slug, :description, :keywords, :admin_id, :created_at, :updated_at
json.url page_url(page, format: :json)
