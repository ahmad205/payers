json.extract! money_op, :id, :opid, :optype, :amount, :payment_gateway, :status, :payment_date, :payment_id, :user_id, :created_at, :updated_at
json.url money_op_url(money_op, format: :json)
