# app/services/facebook_service.rb

class FacebookService

    attr_accessor :message, :post_id
    require 'uri'
    require 'net/http'

    # GET Facebook Post params
    # @param [String] message Facebook post content.
    # @param [Integer] post_id The received Facebook post id.
    def initialize(message:'',post_id:'')
        @message = message
        @post_id = post_id
        @access_token = "EAAfCzkV8a8YBALFtFMr4MiXP6WyxfDAnarJxrd7FJBWv5TZASCXsnfstZACJ0DLVN3BXJGkUQEL3IbRuSjpcI31ZBTjv7zf6adWFqNbANq0opEnGjHLholtUJH8aD5HsZBHSSQ1xZBERNEoYZB2oi1D5NxjXV8pK9YDWi99YJYXjeXxlbFDl5B"
    end

    # Send a post to our facebook page
    # @param [String] message Facebook post content.
    # @param [String] access_token The Conection token between us and our facebook page.
    def send_post

        url = URI("https://graph.facebook.com/283633942236288/feed")
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Post.new(url)
        request["authorization"] = 'AWS4-HMAC-SHA256 Credential=/20180903/us-east-1/execute-api/aws4_request, SignedHeaders=content-length;content-type;host;x-amz-date, Signature=ebb6abcd1f27bb711f04b29467df993ad3c83b39b2b9291f1707d81e997e9a96'
        request["content-type"] = 'application/json'
        request["host"] = 'graph.facebook.com'
        request["x-amz-date"] = '20180903T132044Z'
        request["cache-control"] = 'no-cache'
        request.body = JSON.dump({    
        "message": @message,
        "access_token": @access_token,
        })
        response = http.request(request)
        puts @res = response.read_body
        @applications_list= ActiveSupport::JSON.decode(@res)
        if @applications_list != nil
            Media.create(:admin_id => 1,:media_type => 'facebook',:post_content => @message,:post_id => @applications_list["id"])
        end
        return @applications_list
    end


    # Delete a post from our facebook page
    # @param [Integer] post_id The received Facebook post id.
    # @param [String] access_token The Conection token between us and our facebook page.
    def delete_post

        url = URI("https://graph.facebook.com/#{@post_id}")

        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
        request = Net::HTTP::Delete.new(url)
        request["authorization"] = 'AWS4-HMAC-SHA256 Credential=/20180903/us-east-1/execute-api/aws4_request, SignedHeaders=content-length;content-type;host;x-amz-date, Signature=bb6afc935a0fdf89ac887a7f2395ea2a64550cd2682b791ae4634eb9ec0c973d'
        request["content-type"] = 'application/json'
        request["host"] = 'graph.facebook.com'
        request["x-amz-date"] = '20180903T144024Z'
        request["cache-control"] = 'no-cache'
        request.body = JSON.dump({    
          "access_token": @access_token,
          })
        response = http.request(request)
        puts @res = response.read_body
        @applications_list= ActiveSupport::JSON.decode(@res)

        return @applications_list

    end

end